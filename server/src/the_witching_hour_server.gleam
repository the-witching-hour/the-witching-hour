import app/router
import app/web.{ Context }
import gleam/erlang/process.{ sleep_forever }
import mist.{ new, port, start_http }
import wisp.{ configure_logger, priv_directory, random_string }
import wisp/wisp_mist.{ handler }

pub fn main() {
  configure_logger()

  let assert Ok(_) =
    handler(
        router.handle_request(_, Context(static_directory: static_directory())), 
        random_string(64))
    |> new
    |> port(8000)
    |> start_http

  sleep_forever()
}

fn static_directory() -> String {
    let assert Ok(priv) = priv_directory("the_witching_hour_server")
    priv <> "/static"
}
