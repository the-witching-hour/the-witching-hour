import wisp.{
    type Request,
    type Response,
    handle_head,
    method_override,
    log_request,
    rescue_crashes,
    serve_static,
}

pub type Context {
    Context(static_directory: String)
}

pub fn middleware(
    req: Request,
    ctx: Context,
    handle_request: fn(Request) -> Response,
) -> Response {
    let req = method_override(req)
    use <- log_request(req)
    use <- rescue_crashes
    use req <- handle_head(req)
    use <- serve_static(req, under: "/static", from: ctx.static_directory)

    handle_request(req)
}
