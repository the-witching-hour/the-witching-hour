import app/web.{ type Context }
import gleam/string_tree.{ from_string }
import simplifile.{ read }
import wisp.{
    type Request, 
    type Response,
    html_response,
    not_found,
    path_segments, 
}

pub fn handle_request(req: Request, ctx: Context) -> Response {
    use _req <- web.middleware(req, ctx)

    case path_segments(req) {
        [] -> home_page(ctx) 
        _ -> not_found()
    }
}

fn home_page(ctx: Context) -> Response {
    let assert Ok(index) = read(from: ctx.static_directory <> "/index.html") 
    
    html_response(from_string(index), 200)

}
