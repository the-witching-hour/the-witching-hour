# the_witching_hour

[![Package Version](https://img.shields.io/hexpm/v/the_witching_hour)](https://hex.pm/packages/the_witching_hour)
[![Hex Docs](https://img.shields.io/badge/hex-docs-ffaff3)](https://hexdocs.pm/the_witching_hour/)

```sh
gleam add the_witching_hour@1
```
```gleam
import the_witching_hour

pub fn main() {
  // TODO: An example of the project in use
}
```

Further documentation can be found at <https://hexdocs.pm/the_witching_hour>.

## Development

```sh
gleam run   # Run the project
gleam test  # Run the tests
```
