{
    description = "gleam development environment";
    outputs = { nixpkgs, self }: # Note the use of `self` which allows reusing flake's outputs in itself
    let
        pkgs = import nixpkgs { system = "x86_64-linux"; };
    in
    {
        packages.x86_64-linux.default = with pkgs; [ 
            gleam 
            erlang_26
            inotify-tools
            rebar3 
            wasm-pack 
        ];
        devShells.x86_64-linux.default = pkgs.mkShell {
            packages = [ self.packages.x86_64-linux.default ];
        };
    };
}
