import lustre
import lustre/element.{text}
import lustre/element/html.{p}

pub fn main() {
    let app = lustre.simple(init, update, view)
    let assert Ok(_) = lustre.start(app, "#app", Nil)

    Nil
}

fn init(_flags) {
    0
}

fn update(_model, _message) {
    0
}

fn view(_model) {
    p([], [text("hello, world!")])
}
