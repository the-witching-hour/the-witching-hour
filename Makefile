CLIENT_DIR=$(shell pwd)/client
SERVER_DIR=$(shell pwd)/server
SERVER_STATIC_DIR=$(SERVER_DIR)/priv/static


.PHONY: build-client
build-client:
	cd $(CLIENT_DIR) && \
	gleam run -m lustre/dev build --outdir=$(SERVER_STATIC_DIR) --minify
	cat $(CLIENT_DIR)/index.html | sed -e 's/priv\/static/static/g' >> $(SERVER_STATIC_DIR)/index.html

.PHONY: run-client
run-client:
	cd $(CLIENT_DIR) && gleam run -m lustre/dev start

.PHONY: build-server
build-server:
	cd $(SERVER_DIR) && gleam build

.PHONY: run-server
run-server:
	cd $(SERVER_DIR) && gleam run

.PHONY: run
run:
	$(MAKE) build-client && $(MAKE) run-server
